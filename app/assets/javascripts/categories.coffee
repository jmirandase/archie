# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).ready ->
  $('#search-form')
    .on 'ajax:success', (event, data, status, xhr) ->
      if data
        if $('.search-results .message').length
          $('.search-results').html "<ol class=\"data\">#{data}</ol>"
        else
          $('.search-results .data').html data
      else
        $('.search-results').html '<p class="message">No matches found :(</p>'
    .on 'ajax:error', (event, xhr, status, error) ->
      $('.search-results').html '<p class="message">Oops, something wrong happened. Please try again ;(</p>'

  $('.categories .search-results button')
    .on 'click', (event) ->
      edit_category_path   = "/categories/#{$(@).data('id')}/edit"
      delete_category_path = "/categories/#{$(@).data('id')}"
      if not active_button_exists() or is_toggling $(@)
        $('.actions-bar.animatable').toggleClass('animated slide-up')
      else
        deactivate_current_active_button()
      $(@).toggleClass 'active'
      $(@).closest('.item').toggleClass 'selected'
      $('.actions-bar .action.edit').attr('href', edit_category_path)
      $('.actions-bar .action.delete').attr('action', delete_category_path)

  current_active_button = ->
    $('.categories .search-results button.active')

  active_button_exists = ->
    current_active_button().length

  deactivate_current_active_button = ->
    current_active_button().closest('.item').toggleClass 'selected'
    current_active_button().toggleClass 'active'

  is_toggling = (button) ->
    button[0] is current_active_button()[0]

  $('.categories .metadata .btn-action.add')
    .on 'click', (event) ->
      counter = $('.categories .metadata .form-group').length
      $('.metadata').append(
        "<div class=\"form-group\"> \
           <input class=\"form-control\" placeholder=\"Field Name\" \
                  name=\"category[metadata_attributes][#{counter}][field_name]\" \
                  type=\"text\"> \
           <button name=\"button\" type=\"button\" class=\"btn btn-action remove\" data-id=\"0\"> \
             <span class=\"glyphicon glyphicon-remove\"></span> \
           </button></div>"
      )


  $('.categories .metadata')
    .on 'click', '.btn-action.remove', (event) ->
      container       = $(@).parent()
      is_new_category = $(@).data('id') is 0
      if is_new_category
        remove_from_ui container
      else
        delete_category container

  remove_from_ui = (container) ->
    [..., position] = container.children('input').attr('name').match(/\[(\d+)\]/)
    if position
      inputs                = container.nextAll().children('input')
      update_position = input_position_updater(position)
      update_position $(input) for input in inputs
      container.remove()

  input_position_updater = (position) ->
    position = Number position
    (input) ->
      attr_value = input.attr('name').replace(/\[\d+\]/, "[#{position}]")
      input.attr('name', attr_value)
      position += 1

  delete_category = (container) ->
    [category_id] = container.closest('form').attr('action').match /\d+$/
    field_id      = container.find('.btn-action.remove').data 'id'
    url = "/categories/#{category_id}/fields/#{field_id}"
    $.post url,
           { _method: 'delete' },
           (data) ->
             remove_from_ui container
     .fail ->
       alert 'Oops, something went wrong'


# actions-bar
  $('.actions-bar .action.delete')
    .on 'ajax:success', (event, data, status, xhr) ->
      selected_item = $('.search-results button.active').closest('.item')
      next_item     = selected_item.next()
      next_item.find('button').trigger 'click'
      selected_item.remove()
    .on 'ajax:error', (event, xhr, status, error) ->
      response = JSON.parse xhr.responseText
      alert(response.message)

