class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def index
    if request.post?
      redirect_to categories_path
    end
  end

  def search
    # TODO In the future, it's very likely that we'll implement this
    # functionality as a concern. Inheritance is fine for now.
  end
end
