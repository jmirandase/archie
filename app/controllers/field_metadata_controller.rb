class FieldMetadataController < ApplicationController

  def destroy
    respond_to do |format|
      begin
        @category = RecordType.find params[:category_id]
        @category.metadata.find(params[:id]).destroy
        format.json { render json: { message: 'Field deleted!' }.to_json, status: :ok }
      rescue Exception => e
        message   = "Can't delete. There are records having this field." if e.instance_of? ActiveRecord::DeleteRestrictionError
        message ||= 'Oops! Something unexpected happened.'
        format.json { render json: { message: message }.to_json, status: :sever_error }
      end
    end
  end

end
