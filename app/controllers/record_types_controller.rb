class RecordTypesController < ApplicationController

  layout 'member'

  before_action :filter_out_blank_field_names, only: [:create, :update]

  def new
    @category = RecordType.new
    @category.metadata.build
  end

  def create
    @category = RecordType.new category_params
    if @category.save
      redirect_to categories_path
    else
      render :new
    end
  end

  def index
    @categories = RecordType.all.order :name
  end

  def show
    # TODO 
    @category = RecordType.find params[:id]
  end

  def edit
    @category = RecordType.find params[:id]
  end

  def update
    @category = RecordType.find params[:id]
    if @category.update category_params
      redirect_to categories_path
    else
      render :edit
    end
  end

  def destroy
    respond_to do |format|
      begin
        @category = RecordType.find params[:id]
        @category.destroy
        format.json { render json: { message: 'Record deleted!' }.to_json, status: :ok }
      rescue Exception => e
        message   = "Can't delete. Category has records." if e.instance_of? ActiveRecord::DeleteRestrictionError
        message ||= 'Oops! Something unexpected happened.'
        format.json { render json: { message: message }.to_json, status: :sever_error }
      end
    end
  end

  def search
    @categories = RecordType.where('name like :name', name: "%#{params[:keyword]}%").order :name
    render layout: false
  end


  private

    def category_params
      params.require(:category)
            .permit(:name, metadata_attributes: [ :id,  :field_name ])
    end

    def filter_out_blank_field_names
      if params[:category].has_key? :metadata_attributes
       params[:category][:metadata_attributes].delete_if { |k,v| v[:field_name].blank? }
      end
    end

end
