module ApplicationHelper

  def field_errors model, field
    errors = model.errors
    return unless errors and errors.has_key? field
    items = []
    errors[field].each do |error_message|
      items << %Q|<li class="error">#{error_message}</li>|
    end
    %Q|<ul class="errors">#{items.join ''}</ul>|.html_safe
  end
end
