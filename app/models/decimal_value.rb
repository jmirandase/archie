class DecimalValue < ActiveRecord::Base

  belongs_to :field

  validates :value, presence:     { message: 'Value is required and cannot be empty.' },
                    numericality: { message: "'%{value}' is not a decimal number." }
  validates :field_id, presence: true

end
