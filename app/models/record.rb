class Record < ActiveRecord::Base

  belongs_to :record_type
  has_many   :fields, { dependent: :delete_all }

  validates :record_type_id, presence: true

end
