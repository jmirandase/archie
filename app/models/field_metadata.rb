class FieldMetadata < ActiveRecord::Base

  belongs_to :record_type
  has_many :fields, dependent: :restrict_with_exception

  validates :field_name, presence:   { message: 'Name is required and cannot be empty.' },
                         uniqueness: { scope: :record_type, message: "'%{value}' is already taken. Try a new one.", on: :create },
                         length:     { within: 2..100,
                                       too_short: 'Name is too short. Try again using at least 2 characters.',
                                       too_long: 'Name is too long. Try again using less than 100 characters.'
                                     }

  validates :record_type, presence: true

end
