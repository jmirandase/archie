class Field < ActiveRecord::Base

  # *** TODO ***
  # Implement API to expose a single value attribute for read/write and hide
  # decimal_value, integer_value, string_value, timestamp_value
  has_one :decimal_value, dependent: :delete
  has_one :integer_value, dependent: :delete
  has_one :string_value, dependent: :delete
  has_one :timestamp_value, dependent: :delete
  belongs_to :record
  belongs_to :metadata, class_name: 'FieldMetadata', foreign_key: :field_metadata_id

  validates :record_id, presence: true
  validates :field_metadata_id, presence: true

end
