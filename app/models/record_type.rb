class RecordType < ActiveRecord::Base

  has_many :records, dependent: :restrict_with_exception
  has_many :metadata, inverse_of: :record_type, class_name: 'FieldMetadata', dependent: :delete_all

  validates :name, presence:   { message: 'Name is required and cannot be empty.' },
                   uniqueness: { message: "'%{value}' is already taken. Try a new one." },
                   length:     { within: 4..20,
                                 too_short: 'Name is too short. Try again using at least 4 characters.',
                                 too_long: 'Name is too long. Try again using less than 20 characters.'
                               }

  validate :metadata_presence

  accepts_nested_attributes_for :metadata

  def records_count
    records.count
  end

  private

    def metadata_presence
      errors.add(:metadata, 'At least one field is required.') if metadata.size == 0
    end

end
