class IntegerValue < ActiveRecord::Base

  belongs_to :field

  validates :value, presence:     { message: 'Value is required and cannot be empty.' },
                    numericality: { only_integer: true, message: "'%{value}' is not an integer." }
  validates :field_id, presence: true

end
