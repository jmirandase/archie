class StringValue < ActiveRecord::Base

  belongs_to :field

  validates :value, presence: { message: 'Value is required and cannot be empty.' },
                    length:   { within: 0..65_515,
                                too_long: 'Value is too long. Try again using less than 65515 characters.'
                              }
  validates :field_id, presence: true

end
