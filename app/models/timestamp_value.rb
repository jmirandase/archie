class TimestampValue < ActiveRecord::Base

  belongs_to :field

  validates :value, presence: { message: 'Value is required and cannot be empty.' }
  validates :field_id, presence: true
  validate  :is_a_valid_datetime

  def value= value
    # *** TODO *** let's ignore for now datetime formats and locales
    write_attribute :value, (value.to_datetime rescue nil)
  end

  protected

    def is_a_valid_datetime
      value.to_datetime rescue errors.add :value, "'%{value}' is not a valid date or time value."
    end

end
