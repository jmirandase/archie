Rails.application.routes.draw do

  concern :searchable do
    get :search, on: :collection
  end

  resources :record_types, as: :categories, path: :categories, concerns: :searchable do

    resources :field_metadata, as: :fields, path: :fields, only: :destroy

  end

  root 'application#index', via: [:get, :post] # TODO This needs to be improved, but for now it's OK

end
