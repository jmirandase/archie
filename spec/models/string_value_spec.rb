require 'rails_helper'

RSpec.describe StringValue do
  subject(:value) { FactoryGirl.build :string_value }

  describe 'validations on value' do
    it 'fails with empty or nil values' do
      value.value = nil
      expect(value).to be_invalid
      value.value = ''
      expect(value).to be_invalid
    end

    context 'when value is longer than 65515 characters' do
      it 'is not a valid value' do
        value.value = 'v' * 65_516
        expect(value).to be_invalid
      end
    end

    context 'when value is less than 65516 characters' do
      it 'is a valid value' do
        value.value = 'v' * 65_000
        expect(value).to be_valid
      end
    end
  end

  describe 'validations on field association' do
    context 'when value does not belong to a field' do
      it 'is not a valid value' do
        value.field = nil
        expect(value).to be_invalid
      end
    end

    context 'when value belongs to a field' do
      it 'is a valid value' do
        expect(value).to be_valid
      end
    end
  end
end
