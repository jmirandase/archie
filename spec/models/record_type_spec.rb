require 'rails_helper'

RSpec.describe RecordType do
  subject(:record_type) { FactoryGirl.build :record_type }

  describe 'validations on name' do
    let(:another_record_type) { FactoryGirl.build :record_type }

    it 'fails with empty or nil values' do
      record_type.name = nil
      expect(record_type).to be_invalid
      record_type.name = ''
      expect(record_type).to be_invalid
    end

    it 'passes with non-empty or non-nil values' do
      record_type.name = 'videos'
      expect(record_type).to be_valid
    end

    context 'when name is less than 4 or longer than 20 characters' do
      it 'is not a valid name' do
        record_type.name = 'foo'
        expect(record_type).to be_invalid
        record_type.name = 'foo' * 8
        expect(record_type).to be_invalid
      end
    end

    context 'when name is longer than 3 and less than 21 characters' do
      it 'is a valid name' do
        record_type.name = 'music'
        expect(record_type).to be_valid
        record_type.name = 'family album'
        expect(record_type).to be_valid
      end
    end

    context 'when name has been taken by another record type' do
      it 'is not a valid name' do
        record_type.save!
        another_record_type.name = record_type.name
        expect(another_record_type).to be_invalid
      end
    end

    context 'when name has not been taken by another record type' do
      it 'is a valid name' do
        record_type.save!
        expect(another_record_type).to be_valid
      end
    end
  end

  describe '#destroy' do
    context 'when record type is associated to one or more records' do
      it 'should raise an exception' do
        record_type.save!
        record_type.records << FactoryGirl.build_list(:record, 7)    
        expect { record_type.destroy! }.to raise_error ActiveRecord::DeleteRestrictionError
      end
    end

    context 'when record type is not associated to any record' do
      it 'should not raise an exception' do
        record_type.records.clear
        expect { record_type.destroy! }.to_not raise_error
      end
    end

    context 'when record type has one or more field metadata entries' do
      it 'should delete them all' do
        record_type.save!
        record_type.metadata << FactoryGirl.build_list(:field_metadata, 7)
        ids_of_created_metadata = record_type.metadata.ids
        record_type.destroy!
        expect { FieldMetadata.find ids_of_created_metadata }.to raise_error ActiveRecord::RecordNotFound 
      end
    end
  end
end
