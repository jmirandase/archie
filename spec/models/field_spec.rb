require 'rails_helper'

RSpec.describe Field do
  subject(:field) { FactoryGirl.build :field }

  describe 'validations on record association' do
    context 'when field does not belong to a record' do
      it 'is not a valid field' do
        field.record = nil
        expect(field).to be_invalid
      end
    end

    context 'when field belongs to a record' do
      it 'is a valid field' do
        expect(field).to be_valid
      end
    end
  end

  describe 'validations on field_metadata associations' do
    context 'when field is not associated to field metadata' do
      it 'is not a valid field' do
        field.metadata = nil
        expect(field).to be_invalid
      end
    end

    context 'when field is associated to field metadata' do
      it 'is a valid field' do
        expect(field).to be_valid
      end
    end
  end
end
