require 'rails_helper'

RSpec.describe DecimalValue do
  subject(:value) { FactoryGirl.build :decimal_value }

  describe 'validations on value' do
    it 'fails with empty or nil values' do
      value.value = nil
      expect(value).to be_invalid
      value.value = ''
      expect(value).to be_invalid
    end   
    
    it 'passes with non-emtpy or non-nil values' do
      value.value = 29.0185
      expect(value).to be_valid
    end

    it 'fails with non-decimal values' do
      value.value = 'a string'
      expect(value).to be_invalid
    end

    it 'passes with decimal values' do
      value.value = 19839.05
      expect(value).to be_valid
    end
  end

  describe 'validations on field association' do
    context 'when value does not belong to a field' do
      it 'is not a valid value' do
        value.field = nil
        expect(value).to be_invalid
      end
    end

    context 'when value belongs to a field' do
      it 'is a valid value' do
        expect(value).to be_valid
      end
    end
  end
end
