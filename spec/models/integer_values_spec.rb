require 'rails_helper'

RSpec.describe IntegerValue do
  subject(:value) { FactoryGirl.build :integer_value }

  describe 'validations on value' do
    it 'fails with empty or nil values' do
      value.value = nil
      expect(value).to be_invalid
      value.value = ''
      expect(value).to be_invalid
    end

    it 'passes with non-empty or non-nil values' do
      value.value = 29
      expect(value).to be_valid
    end

    it 'fails with non-integer values' do
      value.value = 'invalid'
      expect(value).to be_invalid
      value.value = 85.01
      expect(value).to be_invalid
    end

    it 'passes with integer values' do
      value.value = 1985
      expect(value).to be_valid
    end
  end

  describe 'validations on field association' do
    context 'when value does not belong to a field' do
      it 'is not a valid value' do
        value.field = nil
        expect(value).to be_invalid
      end
    end

    context 'when value belongs to a field' do
      it 'is a valid value' do
        expect(value).to be_valid
      end
    end
  end
end
