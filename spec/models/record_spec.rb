require 'rails_helper'

RSpec.describe Record do
  subject(:record) { FactoryGirl.build :record  }

  describe 'validations on record_type association' do
    context 'when record does not belong to a record type' do
      it 'is not a valid record' do
        record.record_type = nil
        expect(record).to be_invalid
      end
    end

    context 'when record belongs to a record type' do
      it 'is a valid record' do
        expect(record).to be_valid
      end      
    end
  end

  describe '#destroy' do
    context 'when record is associated to one or more fields' do
      it 'should delete them all' do
        record.save!
        record.fields << FactoryGirl.build_list(:field, 7)
        ids_of_created_fields = record.fields.ids
        record.destroy!
        expect { Field.find ids_of_created_fields }.to raise_error ActiveRecord::RecordNotFound
      end
    end
  end
end
