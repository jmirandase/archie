require 'rails_helper'

RSpec.describe FieldMetadata do
  subject(:metadata) { FactoryGirl.build :field_metadata }

  describe "validations on field name" do
    let(:another_metadata) { FactoryGirl.build :field_metadata, record_type: metadata.record_type }

    it 'fails with empty or nil values' do
      metadata.field_name = nil
      expect(metadata).to be_invalid 
      metadata.field_name = ''
      expect(metadata).to be_invalid 
    end

    it 'passes with non-empty or non-nil values' do
      metadata.field_name = 'foo'
      expect(metadata).to be_valid
    end

    context "when name is less than 2 or longer than 100 characters" do
      it 'is not a valid field name' do
        metadata.field_name = 'a'
        expect(metadata).to be_invalid
        metadata.field_name = 'a' * 101
        expect(metadata).to be_invalid
      end
    end

    context "when name is longer than 1 and less than 101 characters" do
      it 'is a valid field name' do
        metadata.field_name = 'ab'
        expect(metadata).to be_valid
        metadata.field_name = 'a' * 50
        expect(metadata).to be_valid
      end
    end

    context 'when name has been taken by another metadata of the same record type' do
      it 'is not a valid field name' do
        metadata.save!
        another_metadata.field_name = metadata.field_name
        expect(another_metadata).to be_invalid
      end
    end

    context "when name hasn't been taken by another metadata of the same record type" do
      it 'is a valid field name' do
        metadata.save!
        expect(another_metadata).to be_valid
      end
    end
  end

  describe 'validations on record_type association' do
    context 'when field metadata does not belong to a record type' do
      it 'is not valid metadata' do
        metadata.record_type = nil     
        expect(metadata).to be_invalid
      end
    end

    context 'when field metadata belongs to a record type' do
      it 'is valid metadata' do
        expect(metadata).to be_valid
      end
    end
  end

  describe '#destroy' do
    context 'when field metadata is associated to one or more fields' do
      it 'should raise an exception' do
        metadata.fields << FactoryGirl.build_list(:field, 7)
        expect { metadata.destroy! }.to raise_error ActiveRecord::DeleteRestrictionError
      end
    end

    context 'when field metadata is not associated to any field' do
      it 'should not raise an exception' do
        metadata.fields.clear
        expect { metadata.destroy! }.to_not raise_error
      end
    end
  end
end
