require 'rails_helper'

RSpec.describe TimestampValue do
  subject(:value) { FactoryGirl.build :timestamp_value }

  describe 'validations on value' do
    it 'fails whith empty or nil values' do
      value.value = nil
      expect(value).to be_invalid
      value.value = ''
      expect(value).to be_invalid
    end

    it 'fails with invalid date and time values' do
      value.value = '2018-20-30'
      expect(value).to be_invalid
      value.value = '2014-02-31'
      expect(value).to be_invalid
    end

    it 'passes with non-empty, non-nil or valid date and time values' do
      value.value = '2014-02-28'
      expect(value).to be_valid
      value.value = '1985-01-29'
      expect(value).to be_valid
      value.value = '2016-02-29'
      expect(value).to be_valid
    end
  end

  describe 'validations on field association' do
    context 'when value does not belong to a field' do
      it 'is not a valid value' do
        value.field = nil
        expect(value).to be_invalid
      end
    end

    context 'when value belongs to a field' do
      it 'is a valid value' do
        expect(value).to be_valid
      end
    end
  end
end
