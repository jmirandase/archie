FactoryGirl.define do

  factory :field_metadata do
    sequence(:field_name) { |n| "title #{n}" }
    record_type
  end

end
