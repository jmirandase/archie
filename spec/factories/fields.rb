FactoryGirl.define do

  factory :field do
    record
    association :metadata, factory: :field_metadata
  end

end
