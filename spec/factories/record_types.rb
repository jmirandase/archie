FactoryGirl.define do
  factory :record_type do
    sequence(:name) { |n| "books #{n}" }
  end

end
