# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160119210511) do

  create_table "decimal_values", force: :cascade do |t|
    t.decimal  "value",      precision: 65, scale: 4
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "field_id"
  end

  add_index "decimal_values", ["field_id"], name: "index_decimal_values_on_field_id"

  create_table "field_metadata", force: :cascade do |t|
    t.integer  "record_type_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "field_name",     limit: 100
  end

  add_index "field_metadata", ["field_name"], name: "index_field_metadata_on_field_name"
  add_index "field_metadata", ["record_type_id"], name: "index_field_metadata_on_record_type_id"

  create_table "fields", force: :cascade do |t|
    t.integer  "record_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "field_metadata_id"
  end

  add_index "fields", ["field_metadata_id"], name: "index_fields_on_field_metadata_id"
  add_index "fields", ["record_id"], name: "index_fields_on_record_id"

  create_table "integer_values", force: :cascade do |t|
    t.integer  "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "field_id"
  end

  add_index "integer_values", ["field_id"], name: "index_integer_values_on_field_id"

  create_table "record_types", force: :cascade do |t|
    t.string   "name",       limit: 20
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "records", force: :cascade do |t|
    t.integer  "record_type_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "records", ["record_type_id"], name: "index_records_on_record_type_id"

  create_table "string_values", force: :cascade do |t|
    t.string   "value",      limit: 65515
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "field_id"
  end

  add_index "string_values", ["field_id"], name: "index_string_values_on_field_id"

  create_table "timestamp_values", force: :cascade do |t|
    t.datetime "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "field_id"
  end

  add_index "timestamp_values", ["field_id"], name: "index_timestamp_values_on_field_id"

end
