class AddFieldNameToFieldMetadata < ActiveRecord::Migration
  def change
    add_column :field_metadata, :field_name, :string, limit: 100, after: :id
    add_index :field_metadata, :field_name
  end
end
