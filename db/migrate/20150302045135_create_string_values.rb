class CreateStringValues < ActiveRecord::Migration
  def change
    create_table :string_values do |t|
      t.string :value, limit: 65515

      t.timestamps null: false
    end
  end
end
