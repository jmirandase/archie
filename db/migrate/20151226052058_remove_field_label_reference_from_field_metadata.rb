class RemoveFieldLabelReferenceFromFieldMetadata < ActiveRecord::Migration
  def change
    remove_reference :field_metadata, :field_label
  end
end
