class RemoveValueReferenceFromFields < ActiveRecord::Migration
  def change
    change_table :fields do |t|
      t.remove_references :value, polymorphic: true
    end
  end
end
