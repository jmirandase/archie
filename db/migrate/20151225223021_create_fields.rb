class CreateFields < ActiveRecord::Migration
  def change
    create_table :fields do |t|
      t.references :record, index: true
      t.references :field_label, index: true
      t.references :value, after: :field_label_id, polymorphic: true, index: true

      t.timestamps null: false
    end
    add_foreign_key :fields, :records
    add_foreign_key :fields, :field_labels
  end
end
