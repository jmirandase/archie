class AddFieldReferenceToValueTables < ActiveRecord::Migration
  def change
    add_reference :decimal_values, :field, index: true
    add_reference :integer_values, :field, index: true
    add_reference :string_values, :field, index: true
    add_reference :timestamp_values, :field, index: true
  end
end
