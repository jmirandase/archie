class CreateRecords < ActiveRecord::Migration
  def change
    create_table :records do |t|
      t.references :record_type, index: true

      t.timestamps null: false
    end
    add_foreign_key :records, :record_types
  end
end
