class RemoveFieldLabelReferenceFromFields < ActiveRecord::Migration
  def change
    remove_reference :fields, :field_label
  end
end
