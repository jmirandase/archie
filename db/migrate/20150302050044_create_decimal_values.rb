class CreateDecimalValues < ActiveRecord::Migration
  def change
    create_table :decimal_values do |t|
      t.decimal :value, { precision: 65, scale: 4 }

      t.timestamps null: false
    end
  end
end
