class AddFieldMetadataReferenceToFields < ActiveRecord::Migration
  def change
    add_reference :fields, :field_metadata, index: true, after: :record_id
    add_foreign_key :fields, :field_metadata
  end
end
