class CreateFieldMetadata < ActiveRecord::Migration
  def change
    create_table :field_metadata do |t|
      t.references :record_type, index: true
      t.references :field_label, index: true

      t.timestamps null: false
    end
    add_foreign_key :field_metadata, :record_types
    add_foreign_key :field_metadata, :field_labels
  end
end
