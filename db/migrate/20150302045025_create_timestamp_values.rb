class CreateTimestampValues < ActiveRecord::Migration
  def change
    create_table :timestamp_values do |t|
      t.timestamp :value

      t.timestamps null: false
    end
  end
end
